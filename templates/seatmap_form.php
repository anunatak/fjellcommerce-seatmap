<?php foreach ( $posts as $product ) : ?>
			<?php $seatmap = new FCS_Views_Seatmap( $product, $product['key'] ); ?>
			<input type="hidden" value="<?php echo $product['key'] ?>" name="cart_id[<?php echo $product['post']->ID ?>]">
			<h3><?php echo get_the_title( $product['post']->ID ) ?></h3>
			<?php do_action( 'fjellcommerce_seatmap_after_title' ) ?>
			<div class="fjellcommerce-seatmap-container" data-key="<?php echo $product['key'] ?>">
				<p><small><?php echo sprintf( _n( 'Vennligst klikk på setet du ønsker. Du kan velge <strong>%s</strong> sete til dette arrangementet.', 'Vennligst klikk på setene du ønsker. Du kan velge <strong>%s</strong> seter til dette arrangementet', $product['quantity'], 'fjellcommerce' ), $product['quantity'] ) ?></small></p>
				<ul class="fjellcommerce-seatmap-info">
					<li class="fjellcommerce-seatmap-seat"><label></label> <span><?php _e( 'Tilgjengelig sete', 'fjellcommerce-seatmap' ) ?></span></li>
					<li class="fjellcommerce-seatmap-seat fjellcommerce-seatmap-seat-taken"><label></label> <span><?php _e( 'Sete opptatt', 'fjellcommerce-seatmap' ) ?></span></li>
					<li class="fjellcommerce-seatmap-seat fjellcommerce-seatmap-seat-mine"><label></label> <span><?php _e( 'Ditt sete', 'fjellcommerce-seatmap' ) ?></span></li>
				</ul>
				<?php do_action( 'fjellcommerce_seatmap_before_seatmap_open' ) ?>
				<?php $selected_seats = WC()->session->get( 'seatmap_selections', array() ); ?>
				<?php for($i = 0; $i < $product['quantity'];$i++) : ?>
					<input value="<?php echo isset($selected_seats[$product['key']]) ? $selected_seats[$product['key']][$i]['row'] : '' ?>" type="hidden" class="fjellcommerce-seatmap-seat-chosen-value" data-key="<?php echo $product['key'] ?>" data-ticket="<?php echo $i ?>" data-type="row" name="seatmap[<?php echo $product['key'] ?>][<?php echo $i ?>][row]">
					<input value="<?php echo isset($selected_seats[$product['key']]) ? $selected_seats[$product['key']][$i]['seat'] : '' ?>" type="hidden" class="fjellcommerce-seatmap-seat-chosen-value" data-key="<?php echo $product['key'] ?>" data-ticket="<?php echo $i ?>" data-type="seat" name="seatmap[<?php echo $product['key'] ?>][<?php echo $i ?>][seat]">
				<?php endfor; ?>
				<ul class="fjellcommerce-seatmap-chart" data-seats="<?php echo $product['quantity'] ?>">
					<?php do_action( 'fjellcommerce_seatmap_after_seatmap_open' ) ?>
					<?php foreach ( $seatmap->getSeatmap() as $seats ) : ?>
					<li class="fjellcommerce-seatmap-row">
						<ul class="fjellcommerce-seatmap-row-seats">
							<?php foreach ( $seats as $seat ) : ?>
								<li data-row="<?php echo $seat['row'] ?>" data-seat="<?php echo $seat['seat'] ?>" class="fjellcommerce-seatmap-seat <?php echo $seat['mine'] ? 'fjellcommerce-seatmap-seat-mine' : '' ?> <?php echo $seat['taken'] ? 'fjellcommerce-seatmap-seat-taken' : '' ?> <?php echo $seat['disabled'] ? 'fjellcommerce-seatmap-seat-disabled' : '' ?> <?php echo !$seat['disabled'] && !$seat['taken'] ? 'fjellcommerce-seatmap-seat-availiable' : '' ?>">
									<?php if ( $seat['disabled'] || $seat['taken'] ) : ?>
										<input class="fjellcommerce-seatmap-checkbox" disabled="disabled" type="checkbox" id="seatmap-seat-<?php echo $seat['row'] ?>-<?php echo $seat['seat'] ?>">
									<?php else : ?>
										<input
											<?php checked( true, $seat['mine'] ) ?>
											class="fjellcommerce-seatmap-checkbox"
											type="checkbox" id="seatmap-seat-<?php echo $product['key'] ?>-<?php echo $seat['row'] ?>-<?php echo $seat['seat'] ?>"
											name="seat[<?php echo $product['key'] ?>][<?php echo $seat['row'] ?>][<?php echo $seat['seat'] ?>]"
											value="yes">
									<?php endif ?>
									<label for="seatmap-seat-<?php echo $product['key'] ?>-<?php echo $seat['row'] ?>-<?php echo $seat['seat'] ?>"></label>
								</li>
							<?php endforeach ?>
						</ul>
					</li>
					<?php endforeach ?>
				</ul>
			</div>
		<?php endforeach ?>
