<div class="woocommerce fjellcommerce-seatmap">
	<p><?php _e( 'En eller flere av dine valgte arrangementer krever at du velger et sete.', 'fjellcommerce-seatmap' ) ?></p>

	<form action="" method="POST">
		<?php wp_nonce_field( 'fjellcommerce_seatmap' ) ?>
		<?php include_once 'seatmap_form.php' ?>


		<div class="shopping-actions">
			<div class="cart-total">
				<div class="cart-total-title">
					Total sum
				</div>
				<div class="cart-total-number">
					<?php wc_cart_totals_order_total_html() ?>
				</div>
			</div>
			<div class="action-buttons">
				<a href="<?php echo esc_url( home_url( '/program' ) ) ?>" class="nff-button shop-more">Handle videre</a>
				<?php do_action( 'fjellcommerce_seatmap_proceed' ) ?>
			</div>
		</div>

	</form>

</div>
