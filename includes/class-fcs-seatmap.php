<?php
/**
 * Seat maps
 *
 * @class     FCS_Seatmap
 * @version   2.5.0
 * @package   WooCommerce/Classes/Products
 * @category  Class
 * @author    WooThemes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FCS_Seatmap Class.
 */
class FCS_Seatmap {

	/**
	 * Hook in methods.
	 */
	public static function init() {

		if ( !is_admin() ) {
			add_action( 'wp', array( __CLASS__, 'change_next_button' ) );
			add_action( 'wp', array( __CLASS__, 'check_if_needs_seatmap' ) );
			add_action( 'wp', array( __CLASS__, 'process_seatmap' ) );
		}
		add_filter('nff_program_event_ticket_types', array(__CLASS__, 'add_ticket_type'));
		add_filter('fjellcommerce_ticket_array', array(__CLASS__, 'add_seatmap_to_array'), 10, 4);
		add_shortcode( 'fjellcommerce_seatmap', array( __CLASS__, 'shortcode' ) );
		add_action( 'woocommerce_product_options_advanced', array( __CLASS__, 'add_custom_fields' ) );
		add_action( 'woocommerce_process_product_meta', array( __CLASS__, 'save_custom_fields' ) );
		add_action( 'woocommerce_checkout_update_order_meta', array( __CLASS__, 'checkout_meta' ), 10, 2 );
		add_action( 'fjellcommerce_seatmap_proceed', array( 'FCS_Seatmap', 'proceed_button' ), 1 );

		// modify order
		add_action('fjellcommerce_edit_ticket_form', array(__CLASS__, 'edit_ticket_holder_form'), 20, 3);
		add_action('fjellcommerce_edit_ticket_save', array(__CLASS__, 'save_ticket_holder_form'), 20, 4);

		add_action('wp_ajax_fjellcommerce_get_seatmap', array(__CLASS__, 'get_seatmap_ajax'));
	}

	public static function add_ticket_type($types) {
		$types['seatmap'] = 'Salkart';
		return $types;
	}

	/**
	 * For events that have seatmaps enabled we'll need to add seatmaps to the ticket array
	 * @param array $ticket
	 * @param integer $ticket_id
	 * @param integer $item
	 * @param WC_Order $order
	 */
	public static function add_seatmap_to_array($ticket, $ticket_id, $item, $order) {
		if(get_post_meta($ticket['order_item']['product_id'], '_use_seatmaps', true) === 'yes') {
			$seats = get_post_meta( $order->id, '_seatmap_seats', true );
			$ticketID = $ticket['order_ticket'] - 1;
			$data = isset($seats[$ticket['order_item']['variation_id']][$ticketID]) ? $seats[$ticket['order_item']['variation_id']][$ticketID] : array(
				'seat'    => 0,
				'row'   => 0,
			);
			$ticket['seatmap'] = array(
				'seat' => $data['seat'],
				'row' => $data['row']
			);
		}
		return $ticket;
	}

	/**
	 * Saves the seat data for a ticket
	 * @param  array $ticket
	 * @param  integer $ticket_id
	 * @param  integer $order_id
	 * @param  array $postdata
	 * @return void
	 */
	public static function save_ticket_holder_form($ticket, $ticket_id, $order_id, $postdata) {
		$info = $postdata['ticket_info'][$ticket_id];
		$current_info = get_post_meta($order_id, '_seatmap_seats', true);
		if(!$current_info) {
			$current_info = array();
		}
		if(!isset($current_info[$ticket['order_item']['variation_id']])) {
			$current_info[$ticket['order_item']['variation_id']] = array();
		}
		$ticketID = $ticket['order_ticket'] - 1;
		$current_info[$ticket['order_item']['variation_id']][$ticketID] = $info;
		update_post_meta( $order_id, '_seatmap_seats', $current_info );
	}

	/**
	 * Renders a form for editing seats for tickets
	 * @param  array $ticket
	 * @param  integer $ticket_id
	 * @param  WC_Order $order
	 * @return void
	 */
	public static function edit_ticket_holder_form($ticket, $ticket_id, $order) {
		$seats = get_post_meta( $order->id, '_seatmap_seats', true );
		$ticketID = $ticket['order_ticket'] - 1;
		$data = isset($seats[$ticket['order_item']['variation_id']][$ticketID]) ? $seats[$ticket['order_item']['variation_id']][$ticketID] : array(
			'seat'    => 0,
			'row'   => 0,
		);
		?>
		<div class="ticket_edit_part">
			<strong>Salkart</strong>
			<div class="ticket_edit_part_field">
				<div class="ticket_edit_part_label">
					Rad
				</div>
				<div class="ticket_edit_part_input">
					<input type="text" class="widefat" name="ticket_info[<?php echo $ticket_id ?>][row]" value="<?php echo $data['row'] ?>">
				</div>
			</div>
			<div class="ticket_edit_part_field">
				<div class="ticket_edit_part_label">
					Sete
				</div>
				<div class="ticket_edit_part_input">
					<input type="text" class="widefat" name="ticket_info[<?php echo $ticket_id ?>][seat]" value="<?php echo $data['seat'] ?>">
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Renders the proceed button
	 * @return void
	 */
	public static function proceed_button() {
?>
		<button type="submit" class="nff-checkout-button nff-button alt wc-forward">
			<?php echo __( 'Fortsett videre', 'woocommerce' ); ?>
		</button>
		<?php
	}

	/**
	 * Saves the field value from the WC admin screen
	 * @param  integer $post_id
	 * @return void
	 */
	public static function save_custom_fields( $post_id ) {
		$woocommerce_checkbox = isset( $_POST['_use_seatmaps'] ) ? 'yes' : 'no';
		update_post_meta( $post_id, '_use_seatmaps', $woocommerce_checkbox );
	}

	/**
	 * Adds a checkbox to the WC admin screen
	 */
	public static function add_custom_fields() {
		// Print a custom text field
		woocommerce_wp_checkbox( array(
				'id' => '_use_seatmaps',
				'label' => __( 'Use Seatmap', 'fjellcommerce-seatmap' ),
				'description' => __( 'Select whether or not you want to use seatmaps for this product', 'fjellcommerce-seatmap' )
			) );
	}

	/**
	 * Changes the next button so the next step after the cart will be seatmap if needed
	 * @return void
	 */
	public static function change_next_button() {
		if ( self::needs_seatmap() ) {
			remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );
			remove_action( 'woocommerce_proceed_to_checkout', array( 'FCI_Information', 'button_proceed_to_information' ), 20 );
			add_action( 'woocommerce_proceed_to_checkout', array( __CLASS__, 'button_proceed_to_seatmap' ), 20 );
		}
	}

	/**
	 * Checks if we're at checkout and we still need seatmap.
	 *
	 * Redirects if the user is not done.
	 *
	 * @return void
	 */
	public static function check_if_needs_seatmap() {
		if ( is_checkout() && self::does_need_seatmap() ) {
			wp_redirect( self::get_seatmap_url() ); exit;
		}
	}

	/**
	 * Do we still need seatmap from the user?
	 * @return boolean
	 */
	public static function does_need_seatmap() {
		$information = WC()->session->get( 'seatmap_selections', array() );
		return self::needs_seatmap() && !$information;
	}

	/**
	 * Does anything in the cart need seatmap?
	 * @return boolean
	 */
	public static function needs_seatmap() {
		$posts = array_filter( WC()->cart->get_cart(), function( $item ) {
				$post = $item['data']->post;
				if ( $post->_use_seatmaps && $post->_use_seatmaps === 'yes' ) {
					return true;
				}
				return false;
			} );
		if ( count( $posts ) > 0 ) {
			return true;
		}

		return false;
	}

	/**
	 * Renders the button to proceed to entering seatmap
	 * @return void
	 */
	public static function button_proceed_to_seatmap() { ?>
		<a href="<?php echo esc_url( self::get_seatmap_url() ) ;?>" class="seatmap-button nff-checkout-button nff-button alt wc-forward">
			<?php echo __( 'Fortsett til salkart', 'fjellcommerce-seatmap' ); ?>
		</a>
		<?php
	}

	/**
	 * Gets the URL to the seatmap page
	 * @return string
	 */
	public static function get_seatmap_url() {
		return get_permalink( get_option( 'fjellcommerce_seatmap_page', false ) );
	}

	/**
	 * Renders the shortcode for seatmap
	 * @return string
	 */
	public static function shortcode() {
		$posts = array();
		foreach ( WC()->cart->get_cart() as $key => $item ) {
			if($item['data']->post->_use_seatmaps === 'yes') {
				$posts[] = array(
					'post' => $item['data']->post,
					'key' => $key,
					'quantity' => $item['quantity'],
					'item' => $item
				);
			}
		}

		include dirname( FCS_PLUGIN_FILE ) . '/templates/seatmap.php';
		$content = ob_get_clean();

		return $content;
	}

	public static function get_seatmap_ajax() {
		$posts = array();
		foreach ( WC()->cart->get_cart() as $key => $item ) {
			if($item['data']->post->_use_seatmaps === 'yes') {
				$posts[] = array(
					'post' => $item['data']->post,
					'key' => $key,
					'quantity' => $item['quantity'],
					'item' => $item
				);
			}
		}

		include dirname( FCS_PLUGIN_FILE ) . '/templates/seatmap.php';
		die;
	}

	/**
	 * Processes the seatmap supplied by the user
	 * @return void
	 */
	public static function process_seatmap() {
		if ( self::needs_seatmap() && $_POST && isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'fjellcommerce_seatmap' ) ) {

			self::process_seatmap_action();
			$url = apply_filters( 'fjellcommerce_seatmap_proceed_url', wc_get_checkout_url() );
			wp_redirect( $url ); exit;
		}
	}

	public static function process_seatmap_action() {
		$products = $_POST['seatmap'];
		$selections = array();
		foreach ( $products as $cart_id => $tickets ) {
			$cart = WC()->cart->get_cart();
			$cart_item = $cart[$cart_id];
			$selections[$cart_id] = $tickets;
			$selections[$cart_id] = array_slice( $selections[$cart_id], 0, $cart_item['quantity'], true ); // never allow more than the added quantity to be selected
		}
		WC()->session->set( 'seatmap_selections', $selections );
	}

	/**
	 * Saves the seatmap once the order has been placed and resets it.
	 * @param  integer $order_id
	 * @param  array $posted
	 * @return void
	 */
	public static function checkout_meta( $order_id, $posted ) {
		if ( self::needs_seatmap() ) {
			$information = WC()->session->get( 'seatmap_selections', array() );
			$cart = WC()->cart->get_cart();
			$user_info = array();
			$post_ids = array();
			foreach ( $information as $cart_id => $tickets ) {
				$cart_item = $cart[$cart_id];
				$product_id = $cart_item['product_id'];
				$variation_id = isset( $cart_item['variation_id'] ) ? $cart_item['variation_id'] : $cart_item['product_id'];
				$user_info[$variation_id] = $tickets;
				$post_ids[] = $variation_id;
			}
			update_post_meta( $order_id, '_seatmap_seats', $user_info );
			FCS_Views_Seatmap::refreshCache($post_ids);
			WC()->session->set( 'seatmap_selections', array() );
		}
	}


}

FCS_Seatmap::init();
