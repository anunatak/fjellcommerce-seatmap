<?php
/**
 * Seat maps rendering
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * FCS_Seatmap Class.
 */
class FCS_Views_Seatmap {

	/**
	 * Holds the cart item
	 * @var array
	 */
	protected $item;

	/**
	 * Holds the cart id
	 * @var string
	 */
	protected $item_id;

	/**
	 * Set up the seatmap
	 * @param array $item
	 * @param string $item_id
	 */
	public function __construct( $item, $item_id ) {
		$this->item_id = $item_id;
		$this->item = $item;
		$this->taken = $this->takenSeats();
	}

	/**
	 * A static method to refresh the cache of a seatmap
	 * @param  mixed $post_id
	 * @return array
	 */
	public static function refreshCache($post_id) {
		if(!is_array($post_id)) {
			$post_id = array($post_id);
		}
		foreach($post_id as $id) {
			$taken = array();
			$product = wc_get_product($id);
			if(isset($product->variation_id)) {
				$id = $product->parent->id;
			}
			$orders = FC_WooCommerce_API::get_orders_by_product_id($id);
			if($orders) {
				foreach($orders as $order) {
					$order = wc_get_order($order);
					$tickets = FC_Tickets::create_tickets($order->order_key);
					if($tickets) {
						foreach($tickets['tickets'] as $ticket) {
							if(isset($ticket['seatmap']) && $ticket['seatmap']['row'] && $ticket['seatmap']['seat']) {
								$key = $ticket['seatmap']['row'] .'-'. $ticket['seatmap']['seat'];
								$taken[$key] = true;
							}
						}
					}
				}
			}
			update_option('fjellcommerce_seatmap_taken_'. $id, $taken);
		}
		return $taken;
	}

	/**
	 * A static method to refresh the cache of a seatmap
	 * @param  mixed $post_id
	 * @return array
	 */
	public static function refreshCacheOrder($order_id) {
		$taken = array();
		$order = wc_get_order($order_id);
		$tickets = FC_Tickets::create_tickets($order->order_key);
		if($tickets) {
			foreach($tickets['tickets'] as $ticket) {
				if(isset($ticket['seatmap']) && $ticket['seatmap']['row'] && $ticket['seatmap']['seat']) {
					$key = $ticket['seatmap']['row'] .'-'. $ticket['seatmap']['seat'];
					$taken[$key] = true;
				}
			}
		}
		// update_option('fjellcommerce_seatmap_taken_'. $id, $taken);
		return $taken;
	}

	/**
	 * Gets an array of seats that are already taken for this product
	 * @param  boolean $force_update
	 * @return array
	 */
	public function takenSeats( $force_update = false ) {
		if($force_update) {
			self::refreshCache($this->item['post']->ID);
		}
		$taken = get_option('fjellcommerce_seatmap_taken_'. $this->item['post']->ID, array());
		if(!$taken) {
			$taken = self::refreshCache($this->item['post']->ID);
		}
		return $taken;
	}

	/**
	 * Checks if a specific seat has been taken
	 * @param  integer  $row
	 * @param  integer  $seat
	 * @return boolean
	 */
	public function isSeatTaken( $row, $seat ) {
		$takenSeats = $this->taken;
		$key = $row .'-'. $seat;
		return isset( $takenSeats[$key] );
	}

	/**
	 * Checks if a specific seat belongs to the current user
	 * @param  integer  $row
	 * @param  integer  $seat
	 * @return boolean
	 */
	public function isSeatMine( $row, $seat ) {
		$seats = WC()->session->get( 'seatmap_selections', array() );
		if ( isset( $seats[$this->item_id] ) ) {
			$seats = $seats[$this->item_id];
			foreach ( $seats as $place ) {
				if ( $place['row'] == $row && $place['seat'] == $seat )
					return true;
			}
		}
		return false;
	}

	/**
	 * Creates an array of the seatmap for the current product
	 * @return array
	 */
	public function getSeatmap() {
		$seatmap = array();

		$rows = range( 1, 14 );
		$rowSeats = range( 1, 28 );

		foreach ( $rows as $row ) {
			$seats = array();
			$seatNumber = 0;
			foreach ( $rowSeats as $seat ) {
				$disabled = $this->isSeatDisabled( $row, $seat );
				$seatId = 0;
				if ( !$disabled ) {
					$seatNumber++;
					$seatId = $seatNumber;
				}
				$seats[] = array( 'row' => $row, 'seat' => $seatId, 'mine' => $this->isSeatMine( $row, $seatId ), 'disabled' => $disabled, 'taken' => $this->isSeatTaken( $row, $seatId ) );
			}
			$seatmap[] = $seats;
		}
		return $seatmap;
	}

	/**
	 * Creates a list of disabled seats for the seatmap
	 * @return array
	 */
	protected function getDisabledSeats() {
		return array(
			1 => array( 1, 2, 23, 24, 25, 26, 27, 28 ),
			2 => array( 1, 2, 27, 28 ),
			3 => array( 1, 2, 27, 28 ),
			4 => array( 1, 2, 27, 28 ),
			5 => array( 1, 2, 27, 28 ),
			6 => array( 1, 2, 27, 28 ),
			7 => array( 1, 2, 27, 28 ),
			8 => array( 1, 2, 27, 28 ),
			9 => array( 1, 2, 27, 28 ),
			10 => array( 1, 2, 27, 28 ),
			11 => array( 1, 2, 27, 28 ),
			12 => array( 1, 2, 27, 28 ),
			13 => array( 1, 2, 13, 14, 15, 16, 17, 27, 28 ),
			14 => array( 13, 14, 15, 16, 17 )
		);
	}

	/**
	 * Checks if a seat has been disabled
	 * @param  integer  $row
	 * @param  integer  $seat
	 * @return boolean
	 */
	public function isSeatDisabled( $row, $seat ) {
		$disabledSeats = $this->getDisabledSeats();
		if ( isset( $disabledSeats[$row] ) && in_array( $seat, $disabledSeats[$row] ) )
			return true;

		return false;
	}


}
