<?php
/*
Plugin Name: FjellCommerce Seatmap
Description: Integrasjon mot WooCommerce for Norsk Fjellfestival
Plugin URI: http://norskfjellfestival.no
Author: Tor Morten Jensen v/ Anunatak
Author URI: http://anunatak.no
Version: 0.0.1
License: GPL2
Text Domain: fjellcommerce
*/

final class FjellCommerce_Seatmap {

	/**
	 * WooCommerce version.
	 *
	 * @var string
	 */
	public $version = '1.0.0';

	/**
	 * The single instance of the class.
	 *
	 * @var FjellCommerce
	 * @since 1.0
	 */
	protected static $_instance = null;


	/**
	 * Main FjellCommerce Instance.
	 *
	 * Ensures only one instance of FjellCommerce is loaded or can be loaded.
	 *
	 * @since 1.0
	 * @static
	 * @see FjellCommerce()
	 * @return FjellCommerce - Main instance.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'fjellcommerce' ), '2.1' );
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'fjellcommerce' ), '2.1' );
	}

	/**
	 * Auto-load in-accessible properties on demand.
	 *
	 * @param mixed   $key
	 * @return mixed
	 */
	public function __get( $key ) {
		if ( in_array( $key, array( 'payment_gateways', 'shipping', 'mailer', 'checkout' ) ) ) {
			return $this->$key();
		}
	}

	/**
	 * WooCommerce Constructor.
	 */
	public function __construct() {
		if ( $this->dependencies_met() ) {
			$this->define_constants();
			$this->includes();
			$this->init_hooks();

			do_action( 'fjellcommerce_seatmaps_loaded' );
		} else {
			add_action( 'admin_notices', function() { ?>
				<div class="notice notice-error">
					<p><?php _e( '<strong>FjellCommerce Seatmaps</strong> krever at du har <strong>WooCommerce</strong> og <strong>Advanced Custom Fields</strong> installert.', 'fjellcommerce' ); ?></p>
				</div>
			<?php } );
		}
	}

	/**
	 * Check if dependencies are loaded
	 *
	 * @return boolean
	 */
	private function dependencies_met() {
		return function_exists( 'WC' ) && function_exists( 'get_field' );
	}

	/**
	 * Hook into actions and filters.
	 *
	 * @since  1.0
	 */
	private function init_hooks() {
		add_action( 'init', array( $this, 'create_seatmap_page' ) );
	}

	public function create_seatmap_page() {
		if ( !get_option( 'fjellcommerce_seatmap_page', false ) ) {
			$page_id = wp_insert_post( array(
					'post_type' => 'page',
					'post_status' => 'publish',
					'post_content' => '[fjellcommerce_seatmap /]',
					'post_title' => __( 'Seatmap', 'fjellcommerce-seatmap' )
				) );

			update_option( 'fjellcommerce_seatmap_page', $page_id );
		}
	}

	/**
	 * Define WC Constants.
	 */
	private function define_constants() {
		$this->define( 'FCS_PLUGIN_FILE', __FILE__ );
		$this->define( 'FCS_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
		$this->define( 'FCS_VERSION', $this->version );
		$this->define( 'FJELLCOMMERCE_SEATMAP_VERSION', $this->version );
	}

	/**
	 * Define constant if not already set.
	 *
	 * @param string  $name
	 * @param string|bool $value
	 */
	private function define( $name, $value ) {
		if ( ! defined( $name ) ) {
			define( $name, $value );
		}
	}

	/**
	 * What type of request is this?
	 *
	 * @param string  $type admin, ajax, cron or frontend.
	 * @return bool
	 */
	private function is_request( $type ) {
		switch ( $type ) {
		case 'admin' :
			return is_admin();
		case 'ajax' :
			return defined( 'DOING_AJAX' );
		case 'cron' :
			return defined( 'DOING_CRON' );
		case 'frontend' :
			return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
		}
	}

	/**
	 * Include required core files used in admin and on the frontend.
	 */
	public function includes() {
		include_once 'includes/class-fcs-seatmap.php';

		if ( $this->is_request( 'frontend' ) ) {
			include_once 'includes/views/class-fcs-views-seatmap.php';
		}
	}

}

/**
 * Main instance of FjellCommerce.
 *
 * Returns the main instance of WC to prevent the need to use globals.
 *
 * @since  2.1
 * @return FjellCommerce_Seatmap
 */
function FjellCommerce_Seatmap() {
	return FjellCommerce_Seatmap::instance();
}

add_action( 'plugins_loaded', function() {
		// Global for backwards compatibility.
		$GLOBALS['fjellcommerce_seatmap'] = FjellCommerce_Seatmap();
	} );
